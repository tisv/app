import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { ProviderFcmProvider } from '../../providers/provider-fcm/provider-fcm'
import { ProvidersCreateCarProvider } from '../../providers/providers-create-car/providers-create-car';
import { Storage } from "@ionic/storage";
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: "page-pages-main",
  templateUrl: "pages-main.html"
})
export class PagesMainPage {

  public user = {
    person_id: null,
    customer_id: null,
    service_provider_id: null,
    first_name: "",
    last_name: "",
    birth_date: "",
    phone: "",
    email: "",
    password: "",
    confirm_password: "",
    cpf: "",
    is_customer: 0
  };

  public serviceResponse = {
    person_id: "",
    service_provider_id: "",
    first_name: "",
    last_name: "",
    phone: ""
  };

  public opportunity = {
    person_id: "",
    customer_id: "",
    first_name: "",
    last_name: "",
    phone: "",
    car_id: "",
    model: "",
    year: "",
    plate: ""
  }

  public cars = [{
    car_id: 0,
    model: '',
    plate: '',
    year: ''
  }]

  public is_active = false;
  public waitForService = false;
  public serviceFound = false;
  public notifications = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public storage: Storage,
    public fcmProvider: ProviderFcmProvider,
    public CarProvider: ProvidersCreateCarProvider,
    public alertCtrl: AlertController
  ) {

    this.storage.get("user").then(val => {
      this.user = val;
      console.log(this.user);
      this.CarProvider.getCars(this.user.customer_id).then((response => {
        this.cars = JSON.parse(response.body || response._body).result;
        console.log(this.cars);
      }));
    });

  }

  showAlert(title, message) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Oportunidade de serviço',
      message: `${this.opportunity.model} ${this.opportunity.year}`,
      buttons: [
        {
          text: 'Discartar',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Aceitar',
          handler: () => {
            this.unsubscribeFromTopic('serviceRequest');
            this.sendServiceResponse();
          }
        }
      ]
    });
    confirm.present();
  }

  handleActivationButton() {
    if (this.is_active) {
      this.handleNotifications();
      this.subscribeToTopic('serviceRequest');
      this.showAlert('Bom Trabalho!', 'Iremos te notificar quando alguma oportunidade de serviço aparecer!');
    } else {
      this.unsubscribeFromTopic('serviceRequest');
    }
  }

  handleNotifications() {

    this.fcmProvider.onNotification()
      .then((data) => {
        this.handleReceiveMessage(data);
      }).catch((err) => {
        console.error("Error in notification", err)
        this.showAlert("Oops", "Não foi possível completar a solicitação :(");
      })
  }

  handleReceiveMessage(message) {
    if (this.user.is_customer) {
      this.serviceResponse = message;
      this.waitForService = false;
      this.serviceFound = true;
    } else {
      this.opportunity = message;
      this.showConfirm();
    }
  }

  handleSendMessage(topic) {

    let message;

    if (this.user.is_customer) {
      message = {
        payload: {
          notification: {
            title: 'Oportunidade de Serviço',
            body: `${this.cars[0].model} ${this.cars[0].year}`
          },
          data: {
            person_id: `${this.user.person_id}`,
            customer_id: `${this.user.customer_id}`,
            first_name: this.user.first_name,
            last_name: this.user.last_name,
            phone: this.user.phone,
            car_id: `${this.cars[0].car_id}`,
            model: `${this.cars[0].model}`,
            year: `${this.cars[0].year}`,
            plate: this.cars[0].plate
          }
        },
        topic: 'serviceRequest'
      }
    } else {

      message = {
        payload: {
          notification: {
            title: 'Oportunidade de Serviço',
            body: `O lavador ${this.user.first_name} ${this.user.last_name} aceitou sua solicitação.`
          },
          data: {
            person_id: `${this.user.person_id}`,
            service_provider_id: `${this.user.service_provider_id}`,
            first_name: this.user.first_name,
            last_name: this.user.last_name,
            phone: this.user.phone,
          }
        }, topic: topic
      }
    }

    return message;
  }

  handleServiceRequestButton() {

    if (this.user.is_customer == 1 && this.waitForService) {
      return false;
    } else if (this.user.is_customer == 0) {
      return false;
    } else if (this.serviceFound) {
      return false;
    } else {
      return true;
    }
  }

  sendServiceRequest() {

    this.handleNotifications();

    if (this.user.is_customer) {
      if (this.cars.length > 0) {
        this.waitForService = true;
        this.fcmProvider.sendMessage(this.handleSendMessage(null))
          .then((data) => {
            console.log(data);
            this.showAlert("Solicitação Enviada", "Estamos procurando por lavador disponível. Iremos avisa-lo quando encontrarmos!");
            this.subscribeToTopic(`serviceResponse.${this.user.customer_id}`);
          })
      } else {
        this.showAlert("Qual carro?", "Cadastre seu carro antes de solicitar um serviço.");
      }
    }
  }

  sendServiceResponse() {

    if (this.user.is_customer == 0) {
      this.fcmProvider.sendMessage(this.handleSendMessage(`serviceResponse.${this.opportunity.customer_id}`))
        .then((data) => {
          
        })
    }
  }

  subscribeToTopic(topic) {
    this.fcmProvider.subscribeToTopic(topic);
  }

  unsubscribeFromTopic(topic) {
    this.fcmProvider.unsubscribeFromTopic(topic);
  }

  goToPage(page) {
    this.navCtrl.push(page);
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad PagesMainPage");
  }

  logout() {

    this.unsubscribeFromTopic('serviceRequest');
    this.unsubscribeFromTopic(`serviceResponse.${this.user.customer_id}`)

    this.storage.remove("user").then(val => {
      this.navCtrl.popToRoot();
    });
  }

  createCar() {
    this.navCtrl.push('PagesCreateCarPage');
  }

  createCheckingAccount() {
    this.navCtrl.push('PagesCreateCheckingAccountPage');
  }

  editCheckingAccount() {
    this.navCtrl.push('PagesEditCheckingAccountPage');
  }
}
