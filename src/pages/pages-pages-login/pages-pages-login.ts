import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { ProvidersProviderUserProvider } from '../../providers/providers-provider-user/providers-provider-user';

import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';
import { Person } from "../../models/Person";

@IonicPage()
@Component({
  selector: 'page-pages-pages-login',
  templateUrl: 'pages-pages-login.html',
})
export class PagesPagesLoginPage {

  public person: Person;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public ProvidersProviderUserProvider: ProvidersProviderUserProvider,
    public storage: Storage,
    public loadingCtrl: LoadingController) {

      this.person = new Person();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagesPagesLoginPage');
  }

  login() {

  }

}
