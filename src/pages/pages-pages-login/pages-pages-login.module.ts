import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagesPagesLoginPage } from './pages-pages-login';

@NgModule({
  declarations: [
    PagesPagesLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(PagesPagesLoginPage),
  ],
})
export class PagesPagesLoginPageModule {}
