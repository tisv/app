import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProvidersCarProvider } from '../../providers/providers-car/providers-car';

import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-pages-pages-list-car',
  templateUrl: 'pages-pages-list-car.html',
})
export class PagesPagesListCarPage {

  public car = [];

  public user = null;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public providersCar: ProvidersCarProvider,
    public storage: Storage,
    public loadingCtrl: LoadingController) {
    this.storage.get("user").then(val => {
      console.log(val)
      this.user = val;

      this.providersCar.getCars(this.user.id).then(function (response) {
        console.log(response)
      });
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagesPagesListCarPage');
  }

}
