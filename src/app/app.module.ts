import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { IonicStorageModule } from '@ionic/storage';

import { BrMaskerModule } from 'brmasker-ionic-3';
import { ProvidersCreateCheckingAccountProvider } from '../providers/providers-create-checking-account/providers-create-checking-account';
import { ProvidersSearchCheckingAccountProvider } from '../providers/providers-search-checking-account/providers-search-checking-account';
import { ProvidersProviderUserProvider } from '../providers/providers-provider-user/providers-provider-user';
import { ProvidersCarProvider } from '../providers/providers-car/providers-car';


import { FCM } from '@ionic-native/fcm';
import { ProviderFcmProvider } from '../providers/provider-fcm/provider-fcm';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FCM,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ProvidersCreateCheckingAccountProvider,
    ProvidersSearchCheckingAccountProvider,
    ProviderFcmProvider,
    ProvidersProviderUserProvider,
    ProvidersCarProvider
  ]
})
export class AppModule {}
