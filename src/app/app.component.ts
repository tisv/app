import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FCM, NotificationData } from "@ionic-native/fcm";

import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = HomePage;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, private fcm: FCM) {
    platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      //this.initializeApp();
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.fcm.subscribeToTopic('testTopic');

      this.fcm.onNotification().subscribe(
        (data: NotificationData) => {
          if (data.wasTapped) {
            console.log("Received in background", JSON.stringify(data))
            alert(JSON.stringify(data))

          } else {
            console.log("Received in foreground", JSON.stringify(data))
            alert(JSON.stringify(data))
          }
        }, error => {
          console.error("Error in notification", error)
        }
      );

    });
  }

  subscribeToTopic(topic) {
    this.fcm.subscribeToTopic(topic);
  }

  unsubscribeFromTopic(topic) {
    this.fcm.unsubscribeFromTopic(topic);
  }

}

